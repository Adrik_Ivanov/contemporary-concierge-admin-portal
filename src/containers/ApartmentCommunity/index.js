import React, {Component} from 'react';
import ListItem from "../../components/ListItem";
import APIExample from "../../network/apis/APIExample";
import InfiniteScroll from "react-infinite-scroll-component";
import Button from "@material-ui/core/Button";
import NewApartment from "../../components/NewApartment";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";
import CircularProgress from "@material-ui/core/CircularProgress";

class ApartmentCommunity extends Component {

  // this method is only to trigger route guards , remove and use your own logic
  constructor(props) {
    super(props);

    this.state = {
      apartments: [],
      isDialogOpen: false,
      successMsg: false,
      selectedApartment: null,
    }
  }

  componentDidMount() {
    this.refresh()
  }

  refresh() {
    APIExample.fetchCommunities().then(value => {
      this.setState({apartments: value.data});
    }).catch();
  }

  render(){
    const {apartments, isDialogOpen, successMsg, selectedApartment} = this.state;
    return (
      <div className="flex-fill container flex-column d-flex ">
        {isDialogOpen && <NewApartment data={selectedApartment} isOpen={isDialogOpen} onClose={(value) => {
          if (value) {
            this.refresh();
            this.setState({successMsg: value});
          }
          this.setState({isDialogOpen: false, selectedApartment: null});
        }}/>}
        <Snackbar open={successMsg} autoHideDuration={3000} onClose={() => this.setState({successMsg: false})}>
          <Alert onClose={() => this.setState({successMsg: false})} severity={successMsg?.success ? "success" : "error"}>
            {successMsg?.msg}
          </Alert>
        </Snackbar>
        <div className="header row align-items-center justify-content-between">
          <h1>Apartment Community</h1>
        </div>

        <div className="body flex-column justify-content-center" >
          <div className="flex-fill" id="scrollableDiv" style={{height: "calc(100vh - 194px - 2rem)"}}>
            <InfiniteScroll
              className="top-border pb-4"
              scrollableTarget="scrollableDiv"
              dataLength={apartments.length} //This is important field to render the next data
              next={() => {}}
              hasMore={false}
              loader={
                <div className="justify-content-center"><CircularProgress /> </div>
              }
              refreshFunction={() => this.refresh()}
              pullDownToRefresh
              height={"calc(100vh - 194px - 2rem)"}
              pullDownToRefreshThreshold={50}
              pullDownToRefreshContent={
                <h3 style={{textAlign: 'center'}}>&#8595; Pull down to refresh</h3>
              }
              releaseToRefreshContent={
                <h3 style={{textAlign: 'center'}}>&#8593; Release to refresh</h3>
              }>
              {apartments.map(e => <ListItem key={e.id} title={e.name} tailing={e.count + " Tasks"} onTap={() => {
                this.setState({selectedApartment: e, isDialogOpen: true});
              }}/>)}
            </InfiniteScroll>
          </div>
          <div className="w-100 justify-content-center align-items-center d-flex">
            <Button variant="contained" color="primary" size="large" className="w-100 my-3"
                    onClick={() => this.setState({isDialogOpen: true})}>Add Apartment Community</Button>
          </div>
        </div>
      </div>
    )
  }
}

export default ApartmentCommunity;
