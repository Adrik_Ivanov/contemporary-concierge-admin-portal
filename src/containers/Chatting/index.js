import React, {Component} from 'react';
import TextField from "@material-ui/core/TextField";
import Button from '@material-ui/core/Button';
import SendIcon from '@material-ui/icons/Send';
import {auth, database} from "../../network/firebase/firestore";
import {connect} from "react-redux";
import * as firebase from "firebase";
import APIExample from "../../network/apis/APIExample";
import {GiftedChat, Avatar, Bubble, Composer, InputToolbar, Message, MessageText, Send, Time} from "react-web-gifted-chat";

class Chatting extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            messages: [],
            lastDoc: null,
            roomData: this.props.location.state,
            earlyDoc: null,
        }
        this.isClient = !!this.state.roomData?.client_id;
        this.roomID =
            "admin" + '-' + this.state?.roomData.admin_id + '-' + (this.isClient ? this.state?.roomData?.client_id : this.state?.roomData?.employee_id);
        this.adminName =
            (this.state?.roomData.admin_firstname ?? '') +
            ' ' +
            (this.state?.roomData.admin_lastname ?? '');

        if (this.isClient) {
            this.userName =
                (this.state?.roomData.client_firstname ?? '') +
                ' ' +
                (this.state?.roomData.client_lastname ?? '');
        } else {
            this.userName =
                (this.state?.roomData.employee_firstname ?? '') +
                ' ' +
                (this.state?.roomData.employee_lastname ?? '');
        }
        console.log(this.roomID);
        this.db = database.collection('chatRooms').doc(this.roomID);
    }

    componentDidMount() {
        this.refresh();
    }

    componentWillUnmount() {
        if (this.unsubscribe) {
            this.unsubscribe();
        }
    }

    refresh() {
        let query = this.db
            .collection('messages')
            .orderBy('create_at', 'desc')
            .limit(20);
        if (this.state.lastDoc) {
            console.log(this.state.earlyDoc, this.state.lastDoc);
            query = query.startAfter(this.state.lastDoc);
        }
        query.get().then((snap) => {
            this.setState((prev) => ({
                messages: GiftedChat.append(
                    snap.docs.map((e, index) => {
                        const data = e.data();
                        return {
                            createdAt: data?.create_at?.toDate(),
                            text: data?.message ?? '',
                            id: e.id + (index + this.state.messages.length),
                            user: {
                                name:
                                    data?.sender_id === this.state.roomData?.admin_id
                                        ? this.adminName
                                        : this.userName,
                                id: data?.sender_id,
                                avatar:
                                    data?.sender_id === this.state.roomData?.admin_id
                                        ? this.state.roomData?.admin_photo
                                        : this.isClient ? this.state.roomData?.client_photo : this.state.roomData?.employee_photo,
                            },
                        };
                    }),
                    prev.messages,
                ),
                lastDoc: snap.empty ? prev.lastDoc : snap.docs[snap.docs.length - 1],
                earlyDoc: prev.earlyDoc ?? (snap.empty ? null : snap.docs[0]),
            }));
            if (!this.unsubscribe) {
                this.onMessage();
            }
            this.db.update({
                admin_count: 0,
            });
        });
    }

    onMessage() {
        let query = this.db.collection('messages').orderBy('create_at', 'desc');
        if (this.state.earlyDoc) {
            query = query.endBefore(this.state.earlyDoc);
        }
        this.unsubscribe = query.onSnapshot((snap) => {
            this.setState((prev) => ({
                messages: GiftedChat.append(
                    prev.messages,
                    snap.docs.map((e, index) => {
                        const data = e.data();
                        return {
                            createdAt: data?.create_at?.toDate(),
                            text: data?.message ?? '',
                            id: e.id + (index + this.state.messages.length),
                            user: {
                                name:
                                    data?.sender_id === this.state.roomData?.admin_id
                                        ? this.adminName
                                        : this.userName,
                                id: data?.sender_id,
                                avatar:
                                    data?.sender_id === this.state.roomData?.admin_id
                                        ? this.state.roomData?.admin_photo
                                        : this.isClient ? this.state.roomData?.client_photo : this.state.roomData?.employee_photo,
                            },
                        };
                    }),
                ),
                earlyDoc: snap.empty ? prev.earlyDoc : snap.docs[0],
            }));
            if (this.unsubscribe) {
                this.unsubscribe();
            }

            this.db.update(this.isClient ? {
                client_count: 0,
            } : {
                employee_count: 0,
            });
            this.onMessage();
        });
    }

    onSend(messages = []) {
        messages.forEach((e) => {
            if (!e) {
                return;
            }
            console.log(e);
            if (this.isClient) {
                this.db.update({
                    sender_id: e.user.id,
                    last_message: e.text,
                    last_message_type: 'text',
                    last_updated: firebase. firestore.Timestamp.now(),
                    client_count: firebase. firestore.FieldValue.increment(1),
                });
            } else {
                this.db.update({
                    sender_id: e.user.id,
                    last_message: e.text,
                    last_message_type: 'text',
                    last_updated: firebase. firestore.Timestamp.now(),
                    employee_count: firebase. firestore.FieldValue.increment(1),
                });
            }
            this.db.collection('messages').add({
                message: e.text,
                message_type: 'text',
                sender_id: e.user.id,
                create_at: firebase. firestore.Timestamp.now(),
            });
            APIExample.sendFCM({
                user_id: e.user.id,
                message: e.text,
            })
                .then((r) => {
                    console.log(r);
                })
                .catch((err) => console.log(err));
        });
    }

    render(){
        const {roomData} = this.state;
        return(
            <div className="vh-100 container flex-column d-flex ">
                <div className="header row align-items-center justify-content-between">
                    <h1>{this.userName}</h1>
                </div>

                <div className="body flex-column justify-content-center mb-3" >
                    <GiftedChat
                        messages={this.state.messages}
                        onSend={(messages) => this.onSend(messages)}
                        loadEarlier={true}
                        onLoadEarlier={() => this.refresh()}
                        placeholder="Write your message"
                        messagesContainerStyle={{backgroundColor: 'white'}}
                        user={{
                            id: roomData?.admin_id,
                            name: this.adminName,
                            avatar: roomData?.admin_photo ?? 'https://i.pravatar.cc/140',
                        }}
                        renderMessageText={(props) => {
                            return (
                                <MessageText
                                    {...props}
                                    containerStyle={{
                                        left: {
                                            backgroundColor: '#67ADBF',
                                            borderBottomRightRadius: 8,
                                            borderTopLeftRadius: 8,
                                            borderTopRightRadius: 8,
                                            padding: 4,
                                        },
                                        right: {
                                            backgroundColor: '#67BFAA',
                                            borderBottomLeftRadius: 8,
                                            borderTopLeftRadius: 8,
                                            borderTopRightRadius: 8,
                                            padding: 4,
                                        },
                                    }}
                                    textStyle={{
                                        left: {color: 'black'},
                                        right: {color: 'black'},
                                    }}
                                    linkStyle={{
                                        left: {color: 'orange'},
                                        right: {color: 'orange'},
                                    }}
                                    customTextStyle={{fontSize: 18, color: 'white',}}
                                />
                            );
                        }}
                        renderMessage={(props) => <Message {...props} />}
                        renderAvatar={(props) => {
                            return (
                                <Avatar
                                    {...props}
                                    containerStyle={{
                                        left: {marginBottom: 16},
                                        right: {marginBottom: 16},
                                    }}
                                />
                            );
                        }}
                        renderBubble={(props) => {
                            return (
                                <Bubble
                                    {...props}
                                    wrapperStyle={{
                                        left: {
                                            backgroundColor: 'white',
                                            marginTop: 10,
                                        },
                                        right: {
                                            backgroundColor: 'white',
                                            marginTop: 10,
                                        },
                                    }}
                                />
                            );
                        }}
                        timeTextStyle={{
                            right: {
                                color: 'rgba(24, 31, 72, 0.65)',
                                fontSize: 10,
                            },
                            left: {
                                color: 'rgba(24, 31, 72, 0.65)',
                                fontSize: 10,
                            },
                        }}
                        renderTime={(props) => {
                            return (
                                <Time
                                    {...props}
                                    textStyle={{
                                        right: {
                                            color: 'rgba(24, 31, 72, 0.65)',
                                            fontSize: 10,
                                        },
                                        left: {
                                            color: 'rgba(24, 31, 72, 0.65)',
                                            fontSize: 10,
                                        },
                                    }}
                                />
                            );
                        }}
                        text={this.state.text}
                        onInputTextChanged={(text) => this.setState({text})}
                        renderSend={(props) => {
                            return (
                                <Send
                                    {...props}
                                    disabled={!props.text}
                                    containerStyle={{
                                        width: 44,
                                        height: 44,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        marginHorizontal: 4,
                                    }}>
                                    <SendIcon height={19} width={22} color="primary" />
                                </Send>
                            );
                        }}
                        renderComposer={(props) => {
                            return (
                                <Composer
                                    {...props}
                                    placeholder="Write your message"
                                    placeholderTextColor="#A7A9AB"
                                    textInputStyle={{backgroundColor: '#F9F9F9', justifyContent: 'center',}}
                                />
                            );
                        }}
                        renderInputToolbar={(props) => {
                            return (
                                <InputToolbar
                                    {...props}
                                    containerStyle={{
                                        height: 52,
                                        backgroundColor: '#F9F9F9',
                                        justifyContent: 'center',
                                        paddingHorizontal: 12,
                                    }}
                                />
                            );
                        }}
                    />
                </div>
                {/*<div className="flex-row message-input d-flex align-items-center ">*/}
                {/*    <TextField*/}
                {/*        id="message-input"*/}
                {/*        placeholder="Write your message"*/}
                {/*        className="my-1 message-input flex-fill"*/}
                {/*        onChange={(event) => this.handleSendMessage(event.target.value)}*/}
                {/*    />*/}
                {/*    <Button*/}
                {/*        variant="contained"*/}
                {/*        color="primary"*/}
                {/*        className="ml-3"*/}
                {/*        endIcon={<SendIcon />}*/}
                {/*    >*/}
                {/*        Send*/}
                {/*    </Button>*/}
                {/*</div>*/}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user : state.auth.user
    }
}

export default connect(mapStateToProps, null)(Chatting);
