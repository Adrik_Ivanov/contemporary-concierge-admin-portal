import React, {Component} from 'react';
import InfiniteScroll from "react-infinite-scroll-component";
import API from "../../network/apis/APIExample";
import CustomerItem from "../../components/CustomerItem";
import CircularProgress from "@material-ui/core/CircularProgress";
import Button from "@material-ui/core/Button";
import {navHistory} from "../../routes/History";
import InviteCustomerDialog from "../../components/InviteCustomerDialog";
import Alert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";
import DeleteIcon from '@material-ui/icons/Delete';
import Moment from "moment";
import IconButton from "@material-ui/core/IconButton";

class Customer extends Component {

    // this method is only to trigger route guards , remove and use your own logic
    constructor(props) {
        super(props);

        this.state = {
            items: [],
            next_url: '',
            prev_url: '',
            total: 0,
            successMsg: false,
            isOpenDialog: false,
        }
    }

    componentDidMount() {
        this.fetchData()
    }

    fetchData(next) {
        const {items} = this.state;
        if (next) {
            API.fetchInvitedCustomers('?limit=10&offset=' + items.length).then((value) =>{
                const {next, previous, results} = value.data;
                this.setState((prev) => ({items: [...prev.items, ...results], next_url: next, prev_url: previous}));
            });
        } else {
            API.fetchInvitedCustomers('').then((value) => {
                const {count, next, previous, results} = value.data;
                this.setState({items: results, total: count, next_url: next, prev_url: previous});
            });
        }
    }

    resendInvite(id) {
        API.resendInviteCustomer(id).then(_ => this.setState({successMsg: {
                success: true,
                msg: 'The invitation was sent again successfully.'
            }})).catch(_ => this.setState({successMsg: {
                success: false,
                msg: 'You request was failed. Please try again later.'
            }}));
    }

    deleteInvitation(id) {
        API.deleteInviteCustomer(id).then(_ => {
            this.setState({successMsg: {
                    success: true,
                    msg: 'The invitation was deleted successfully.'
                }});
            this.fetchData(false);
        }).catch(_ => this.setState({successMsg: {
                success: false,
                msg: 'You request was failed. Please try again later.'
            }}));
    }

    renderItem(item) {
        return (
            <div key={item?.id} className="row d-flex mx-0 align-items-center task-item">
                <div className="col-2 flex-row d-flex align-items-center">
                    <h2 className='text-center'>{item?.name}</h2>
                </div>
                <div className="col-2">
                    <h2 className='text-center'>{item?.address}</h2>
                </div>
                <div className="col-2">
                    <h2 className='text-center'>{item?.mobile_number}</h2>
                </div>
                <div className="col-2">
                    <h2 className='text-center'>{item?.email}</h2>
                </div>
                <div className="col-2">
                    <h2 className='text-center'>{Moment(item?.created_at ?? '').format("MMM DD, yyyy")}</h2>
                </div>
                <div className="col-2 d-flex flex-row">
                    <Button variant="contained" color="primary" size="small" onClick={() => this.resendInvite(item?.id)}>Resend Invite</Button>
                    <IconButton color="secondary" aria-label="delete" className="ml-3" onClick={() => this.deleteInvitation(item?.id)}>
                        <DeleteIcon />
                    </IconButton>
                </div>
            </div>
        );
    }

    render(){
        const {items, total, successMsg, isOpenDialog} = this.state;
        return (
            <div className="flex-fill container flex-column d-flex ">
                <InviteCustomerDialog isOpen={isOpenDialog} onClose={(value) => {
                    if (value) {
                        this.fetchData(false);
                        this.setState({successMsg: value});
                    }
                    this.setState({isOpenDialog: false});
                }}/>
                <div className="header row align-items-center justify-content-between">
                    <h1>Invite Customers</h1>
                </div>
                <Snackbar open={successMsg} autoHideDuration={3000} onClose={() => this.setState({successMsg: false})}>
                    <Alert onClose={() => this.setState({successMsg: false})} severity={successMsg?.success ? "success" : "error"}>
                        {successMsg?.msg}
                    </Alert>
                </Snackbar>
                <div className="body flex-column justify-content-center" >
                    <div className="flex-fill" id="scrollableDiv" style={{height: "calc(100vh - 194px - 2rem)"}}>
                        <div className="row d-flex mx-0 align-items-center task-item-header">
                            <div className="col-2">
                                <h2 className='text-center'>CUSTOMER NAME</h2>
                            </div>
                            <div className="col-2">
                                <h2 className='text-center'>ADDRESS</h2>
                            </div>
                            <div className="col-2">
                                <h2 className='text-center'>CONTACT NO</h2>
                            </div>
                            <div className="col-2">
                                <h2 className='text-center'>EMAIL ID</h2>
                            </div>
                            <div className="col-2">
                                <h2 className='text-center'>LEASE END DATE</h2>
                            </div>
                            <div className="col-2" />
                        </div>
                        <InfiniteScroll
                            scrollableTarget="scrollableDiv"
                            dataLength={items.length} //This is important field to render the next data
                            next={() => this.fetchData(true)}
                            hasMore={total > items.length}
                            loader={
                                <div className="justify-content-center"><CircularProgress /> </div>
                            }
                            height={"calc(100vh - 284px - 2rem)"}
                            endMessage={
                                <p style={{textAlign: 'center'}}>
                                    <b>Yay! You have seen it all</b>
                                </p>
                            }
                            refreshFunction={() => this.fetchData()}
                            pullDownToRefresh
                            pullDownToRefreshThreshold={50}
                            pullDownToRefreshContent={
                                <h3 style={{textAlign: 'center'}}>&#8595; Pull down to refresh</h3>
                            }
                            releaseToRefreshContent={
                                <h3 style={{textAlign: 'center'}}>&#8593; Release to refresh</h3>
                            }>
                            {items.map((e, index) => this.renderItem(e))}
                        </InfiniteScroll>
                        <div className="w-100 justify-content-center align-items-center d-flex">
                            <Button variant="contained" color="primary" size="large" className="w-100 my-3"
                                    onClick={() => this.setState({isOpenDialog: true})}>Invite Customer</Button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Customer;
