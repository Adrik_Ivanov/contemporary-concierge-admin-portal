import React, {Component} from 'react';
import {navHistory} from '../../routes/History';
import IconButton from "@material-ui/core/IconButton";
import ArrowBackIos from "@material-ui/icons/ArrowBackIos";
import Star from "@material-ui/icons/Star";
import Place from "@material-ui/icons/Place";
import Assignment from "@material-ui/icons/Assignment";
import {Avatar} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Switch from "@material-ui/core/Switch";
import TaskerRecentItem from "../../components/TaskerRecentItem";
import APIExample from "../../network/apis/APIExample";
import Alert from "@material-ui/lab/Alert";
import {database} from "../../network/firebase/firestore";
import {connect} from "react-redux";

class CustomerProfile extends Component {

  constructor(props, context) {
    super(props, context);

    this.state = {
      garbageRemoval: this.props.location.state.is_garbage_removal,
      categories: this.props.location.state.is_Concierge,
      tasks: []
    }
    this.db = database;
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    const customer = this.props.location.state;
    APIExample.fetchRecentTasks(customer.user.id).then((value) => this.setState({tasks: value.data}))
  }

  updateData(param) {
    const customer = this.props.location.state;
    APIExample.updateCustomer(customer.id, '?option=' + param)
      .then(value => console.log(value)).catch(error => console.log(error));
  }

  goToChatRoom(customer) {
    const {user} = this.props;
    const roomId =
        "admin" + '-' + user.id + '-' + customer?.user?.id;
    this.db
        .collection('chatRooms')
        .doc(roomId)
        .get()
        .then((value) => {
          if (value.exists) {
            navHistory.push('chatting', value.data());
          } else {
            const room = {
              admin_id: user?.id,
              admin_firstname: user?.first_name,
              admin_lastname: user?.last_name,
              admin_photo: user?.profile?.photo,
              client_id: customer?.user?.id,
              client_firstname: customer?.user?.first_name,
              client_lastname: customer?.user?.last_name,
              client_photo: customer?.photo,
              enable: true,
              sender_id: null,
              last_message: null,
              last_message_type: null,
              last_updated: null,
            };
            this.db
                .collection('chatRooms')
                .doc(roomId)
                .set(room)
                .then((r) =>
                    navHistory.push('chatting', room)
                );
          }
        });

  }

  render(){
    const {garbageRemoval, categories, tasks} = this.state;
    const customer = this.props.location.state;
    return(
      <div className="container row">
        <div className="col-md-6 col-12 d-flex flex-column">
          <div className="header align-items-center d-flex flex-row">
            <IconButton aria-label="delete" color="primary" onClick={() => navHistory.replace('/customer')}>
              <ArrowBackIos className="dark-color" />
            </IconButton>
            <h1 className="mb-0">{customer?.user?.first_name} {customer?.user?.last_name}</h1>
          </div>
          <div className="flex-column d-flex round-border pt-5">
            <div className="justify-content-center d-flex">
              <Avatar className="profile-avatar" src={customer?.photo}/>
            </div>
            <div className="justify-content-center d-flex mt-3">
              <h1>{customer?.user?.first_name} {customer?.user?.last_name}</h1>
            </div>
            <div className="justify-content-center d-flex">
              <h3>{customer?.mobile_number}</h3>
            </div>
            <div className="justify-content-center d-flex">
              <h3>{customer?.user?.email}</h3>
            </div>
            <div className="justify-content-between d-flex my-4 px-4 mx-5">
              <div className="row">
                <Star className="tiny-icon warning-color mr-1"/>
                <h3>5.0</h3>
              </div>
              <div className="row">
                <Place className="tiny-icon primary-color mr-1"/>
                <h3>2111 Austin street</h3>
              </div>
              <div className="row">
                <Assignment className="tiny-icon primary-color mr-1"/>
                <h3>12 tasks</h3>
              </div>
            </div>
            <div className="justify-content-center d-flex">
              {/*<Button className="width-148 mx-3" size="large" color="primary" variant="contained">*/}
              {/*  Call*/}
              {/*</Button>*/}
              <Button onClick={() => this.goToChatRoom(customer)} className="width-148 mx-3 primary-color" size="large" color="primary" variant="outlined">
                Message
              </Button>
            </div>
            <div className="border-top mx-3 mt-4 py-3 px-2">
              <div className="justify-content-between d-flex flex-row">
                <h2>Garbage removal service</h2>
                <Switch
                  color="primary"
                  checked={garbageRemoval}
                  onChange={(event) => {
                    this.setState({garbageRemoval: event.target.checked});
                    this.updateData('gr');
                  }}
                  name="checkedC" />
              </div>
            </div>
            <div className="border-top mx-3 py-3 px-2">
              <div className="justify-content-between d-flex flex-row">
                <h2>Concierge Services</h2>
                <Switch
                  color="primary"
                  checked={categories}
                  onChange={(event) => {
                    this.setState({categories: event.target.checked});
                    this.updateData('ic');
                  }}
                  name="checkedC" />
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-6 col-12" >
          <div className="header align-items-center d-flex">
            <h1 className="mb-0">Recent tasks</h1>
          </div>
          <div className="flex-column d-flex round-border px-3">
            <div className="row d-flex mx-0 align-items-center task-item-header">
              <div className="col-6">
                <h2 className='text-center'>TASKER</h2>
              </div>
              <div className="col-6">
                <h2 className='text-center'>TASK</h2>
              </div>
            </div>
            {tasks.map(e => <TaskerRecentItem key={e.id} item={e}/>)}
            {!tasks.length && <div className="my-3">
              <h2 className="text-center">Nothing</h2>
            </div>}
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.auth.user
  }
}

export default connect(mapStateToProps, null)(CustomerProfile);
