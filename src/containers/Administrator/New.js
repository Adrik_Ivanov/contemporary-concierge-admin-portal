import React, {Component} from 'react';
import TextField from "@material-ui/core/TextField";
import SimpleReactValidator from "simple-react-validator";
import {KeyboardDatePicker} from "@material-ui/pickers";
import Button from "@material-ui/core/Button";
import APIExample from "../../network/apis/APIExample";
import Moment from "moment";
import Alert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";

class NewProfile extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            email: '',
            firstName: '',
            lastName: '',
            password: '',
            passwordAgain: '',
            bloodGroup: '',
            address: null,
            phoneNumber: ''
        }

        this.validator = new SimpleReactValidator();
    }


    // this method is only to trigger route guards , remove and use your own logic
    handleSubmit = () => {

        if (this.validator.allValid()) {
            const {email, firstName, lastName, password, address, phoneNumber} = this.state;
            APIExample.createAdmin({
                first_name: firstName,
                last_name: lastName,
                email: email,
                password: password,
                phone: phoneNumber,
                address: address,
                successMsg: null
            }).then((_) => {
                this.setState({
                    first_name: '',
                    last_name: '',
                    email: '',
                    password: '',
                    phone: '',
                    address: '',
                    successMsg: {
                        msg: "A Administrator was registered successfully.",
                        success: true,
                    }
                })
            }).catch((_) => {
                this.setState({
                    successMsg: {
                        msg: "Administrator registration was failed.",
                        success: false,
                    }
                })
            });
        } else {
            this.validator.showMessages();
            this.forceUpdate();
        }
    }

    render(){
        const {email, firstName, lastName, password, passwordAgain, phoneNumber, address, successMsg} = this.state;
        return(
            <div className="flex-fill container flex-column d-flex">
                <Snackbar open={!!successMsg} autoHideDuration={3000} onClose={() => this.setState({successMsg: null})}>
                    <Alert onClose={() => this.setState({successMsg: null})} severity={successMsg?.success ? "success" : "error"}>
                        {successMsg?.msg}
                    </Alert>
                </Snackbar>
                <div className="header row align-items-center justify-content-between">
                    <h1>Add Administrator</h1>
                </div>
                <div className="justify-content-center row d-flex" >
                    <div className="col-sm-6 col-12 mb-5">
                        <h3>FIRST NAME</h3>
                        <TextField
                            id="first-name-input"
                            placeholder="Enter first name"
                            className="w-100 mt-0"
                            value={firstName}
                            onChange={(event) => this.setState({firstName: event.target.value})}
                        />
                        {this.validator.message('first name', firstName, 'required', { className: 'text-danger' })}
                    </div>
                    <div className="col-sm-6 col-12 mb-5">
                        <h3>LAST NAME</h3>
                        <TextField
                            id="last-name-input"
                            placeholder="Enter last name"
                            className="w-100 mt-0"
                            value={lastName}
                            onChange={(event) => this.setState({lastName: event.target.value})}
                        />
                        {this.validator.message('last name', lastName, 'required', { className: 'text-danger' })}
                    </div>
                    <div className="col-sm-6 col-12 mb-5">
                        <h3>EMAIL ID</h3>
                        <TextField
                            id="email-address-input"
                            placeholder="Enter admin email"
                            className="w-100 mt-0"
                            value={email}
                            onChange={(event) => this.setState({email: event.target.value})}
                        />
                        {this.validator.message('email', email, 'required|email', { className: 'text-danger' })}
                    </div>
                    <div className="col-sm-6 col-12 mb-5">
                        <h3>CONTACT NUMBER</h3>
                        <TextField
                            id="phone-number-input"
                            placeholder="Enter admin contact number"
                            className="w-100 mt-0"
                            value={phoneNumber}
                            onChange={(event) => this.setState({phoneNumber: event.target.value})}
                        />
                        {this.validator.message('contact number', lastName, 'required', { className: 'text-danger' })}
                    </div>
                    <div className="col-sm-6 col-12 mb-5">
                        <h3>PASSWORD</h3>
                        <TextField
                            id="password-input"
                            placeholder="Create password"
                            type="password"
                            className="w-100 mt-0"
                            value={password}
                            onChange={(event) => this.setState({password: event.target.value})}
                        />
                        {this.validator.message('first name', password, 'required', { className: 'text-danger' })}
                    </div>
                    <div className="col-sm-6 col-12 mb-5">
                        <h3>CONFIRM PASSWORD</h3>
                        <TextField
                            id="password-again-input"
                            type="password"
                            placeholder="Repeat your password"
                            className="w-100 mt-0"
                            value={passwordAgain}
                            onChange={(event) => this.setState({passwordAgain: event.target.value})}
                        />
                        {this.validator.message('confirm password', passwordAgain, `required|in:${password}`, { className: 'text-danger' })}
                    </div>
                    <div className="col-sm-6 col-12 mb-5">
                        <h3>Address</h3>
                        <TextField
                            id="admin-address"
                            placeholder="Enter admin address"
                            className="w-100 mt-0"
                            value={address}
                            onChange={(event) => this.setState({address: event.target.value})}
                        />
                        {this.validator.message('address', address, 'required', { className: 'text-danger' })}
                    </div>
                    <div className="col-sm-6 col-12 mb-5">
                        {/*<h3>BLOOD GROUP</h3>*/}
                        {/*<Select*/}
                        {/*  labelId="blood-group"*/}
                        {/*  id="blood-group-id"*/}
                        {/*  placeholder="Repeat tasker blood group"*/}
                        {/*  className="w-100 mt-0"*/}
                        {/*  value={bloodGroup}*/}
                        {/*  onChange={(event) => this.setState({bloodGroup: event.target.value})}*/}
                        {/*>*/}
                        {/*  <MenuItem value="" disabled>Placeholder</MenuItem>*/}
                        {/*  <MenuItem value="A">A</MenuItem>*/}
                        {/*  <MenuItem value="B">B</MenuItem>*/}
                        {/*  <MenuItem value="AB">AB</MenuItem>*/}
                        {/*  <MenuItem value="O">O</MenuItem>*/}
                        {/*</Select>*/}
                        {/*{this.validator.message('blood group', bloodGroup, 'required', { className: 'text-danger' })}*/}
                    </div>
                    <div className="w-100 mx-3">
                        <Button variant="contained" color="primary" size="large" className="w-100 my-3"
                                onClick={this.handleSubmit}>Add Administrator</Button>
                    </div>
                </div>
            </div>
        )
    }
}


export default NewProfile;
