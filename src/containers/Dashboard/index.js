import React, {Component} from 'react';
import SwipeableViews from 'react-swipeable-views';
import {navHistory} from '../../routes/History';
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import TabPanel from '../../components/TabPanel'
import {connect} from "react-redux";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import Alert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";

class Dashboard extends Component {

  constructor(props) {
    super(props);

    this.state = {
      selectedTab: 0,
      selectedCategory: -1,
      result: null,
    }
  }

  // this method is only to trigger route guards , remove and use your own logic
  handleChangeTab = (event, newValue) => {
    this.setState({selectedTab: newValue});
  }

  handleChangeIndex = (index) => {
    this.setState({selectedTab: index});
  };

  render() {
    const {selectedTab, selectedCategory, result} = this.state;
    const {apartments} = this.props;
    let category = '';
    if (selectedCategory === 0) {
      category = '&apartment=0';
    } else if (selectedCategory?.id) {
      category = '&apartment=' + selectedCategory?.id;
    }
    return(
      <div className="flex-fill container">
        <Snackbar open={!!result} autoHideDuration={3000} onClose={() => this.setState({successMsg: false})}>
          <Alert onClose={() => this.setState({result: false})} severity={result?.success ? "success" : "error"}>
            {result?.msg}
          </Alert>
        </Snackbar>
        <div className="header row align-items-center justify-content-between">
          <Tabs
            value={selectedTab}
            onChange={this.handleChangeTab}
            indicatorColor="primary"
            textColor="primary"
            centered
          >
            <Tab label="In Progress" />
            <Tab label="Pending" />
            <Tab label="Unassigned" />
            <Tab label="Completed" />
          </Tabs>
          <Select
            className="width-148 overflow-hidden"
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={selectedCategory}
            onChange={(event) => this.setState({selectedCategory: event.target.value})}
          >
            <MenuItem value={0}>All</MenuItem>
            {apartments.map(e => <MenuItem key={e.id} value={e}>{e?.name}</MenuItem>)}
          </Select>
        </div>
        <div className="body flex-row justify-content-center">
          <div className="row d-flex mx-0 align-items-center task-item-header">
            <div className="col-2">
              <h2 className='text-center'>CUSTOMER NAME</h2>
            </div>
            <div className="col-2">
              <h2 className='text-center'>TASK</h2>
            </div>
            <div className="col-2">
              <h2 className='text-center'>Time</h2>
            </div>
            <div className="col-2">
              <h2 className='text-center'>SERVICE REQUEST</h2>
            </div>
            <div className="col-2">
              <h2 className='text-center'>ADDRESS</h2>
            </div>
            <div className="col-2">
              <h2 className='text-center'>TASKER</h2>
            </div>
          </div>
          <SwipeableViews
            index={selectedTab}
            onChangeIndex={this.handleChangeIndex}>
            <TabPanel value={selectedTab} index={0} query={'?status=in_progress' + (category ?? '')}/>
            <TabPanel value={selectedTab} index={1} query={'?status=pending' + (category ?? '')}/>
            <TabPanel value={selectedTab} index={2} refresh={(value) => {
              if (result?.success) {
                this.setState({result: value});
              }
            }} query={'?status=unassigned' + (category ?? '')}/>
            <TabPanel value={selectedTab} index={3} query={'?status=completed' + (category ?? '')}/>
          </SwipeableViews>
          <div className="w-100 justify-content-center align-items-center d-flex">
            <Button variant="contained" color="primary" size="large" className="w-100 my-3"
                    onClick={() => navHistory.push('/tasks')}>View all</Button>
          </div>
        </div>
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    categories: state.auth.categories,
    apartments: state.auth.apartments
  }
}

export default connect(mapStateToProps, null)(Dashboard);
