import React, {Component} from 'react';
import {connect} from 'react-redux';
import Drawer from "@material-ui/core/Drawer";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import History, {navHistory} from "../../routes/History";
import {drawerRoutes} from "../../routes/Routes";
import {Router} from "react-router-dom";
import NavUser from "../../components/NavUser";
import {requestTaskCategory, requestLogout, featureApartmentRequest} from "../../store/actions/Feature1";
import Button from "@material-ui/core/Button";
import {auth} from "../../network/firebase/firestore";

class Home extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const {requestCategory, requestApartments} = this.props;
        requestCategory();
        requestApartments();
        auth.signInAnonymously().then(e => console.log(e));
    }

    render() {
        const {user, requestLogout} = this.props;
        return (
            <div className="d-flex">
                <Drawer
                    className="nav-drawer"
                    variant="permanent"
                    classes={{paper: "drawer"}}
                    anchor="left"
                >
                    <NavUser user={user}/>
                    <Divider/>
                    <List>
                        <ListItem button key='dashboard' onClick={() => navHistory.push('/')}>
                            <ListItemText primary="Dashboard"/>
                        </ListItem>
                        <ListItem button key='messages' onClick={() => navHistory.push('/message')}>
                            <ListItemText primary="Messages"/>
                        </ListItem>
                        <ListItem button key='customers' onClick={() => navHistory.push('/customer')}>
                            <ListItemText primary="Customers"/>
                        </ListItem>
                        <ListItem button key='invite-customers' onClick={() => navHistory.push('/invite-customer')}>
                            <ListItemText primary="Invite Customers"/>
                        </ListItem>
                        <ListItem button key='administrators' onClick={() => navHistory.push('/administrator')}>
                            <ListItemText primary="Administrators"/>
                        </ListItem>
                        <ListItem button key='taskers' onClick={() => navHistory.push('/tasker')}>
                            <ListItemText primary="Taskers"/>
                        </ListItem>
                        {/*<ListItem button key='settings' onClick={() => navHistory.push('/setting')}>*/}
                        {/*  <ListItemText primary="Settings" />*/}
                        {/*</ListItem>*/}
                        <ListItem button key='task-categories' onClick={() => navHistory.push('/category')}>
                            <ListItemText primary="Task Categories"/>
                        </ListItem>
                        <ListItem button key='apartment-community'
                                  onClick={() => navHistory.push('/apartment-community')}>
                            <ListItemText primary="Apartment Community"/>
                        </ListItem>
                        <ListItem button key='logout' onClick={() => {
                            requestLogout()
                            History.replace('/');
                        }}>
                            <ListItemText primary="Logout"/>
                        </ListItem>
                    </List>
                    <Button variant="contained" color="primary" size="large"
                            className="w-100 my-3 mx-auto button-border"
                            onClick={() => navHistory.push('new_tasker')}>Add tasker</Button>
                </Drawer>
                <main className="flex-fill vh-100">
                    <Router history={navHistory}>
                        {drawerRoutes}
                    </Router>
                </main>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.auth.user
    }
}

const mapDispatchToProps = dispatch => ({
    requestCategory: () => {
        dispatch(requestTaskCategory());
    },
    requestApartments: () => {
        dispatch(featureApartmentRequest());
    },
    requestLogout: () => {
        dispatch(requestLogout())
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
