import React, {Component} from 'react';
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Input from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import {Visibility, VisibilityOff} from "@material-ui/icons";
import {connect} from "react-redux";
import {adminLogin} from "../../store/actions/Feature1";
import SimpleReactValidator from "simple-react-validator";
import NewApartment from "../../components/NewApartment";
import InviteCustomerDialog from "../../components/InviteCustomerDialog";

class Login extends Component {

  constructor(props) {
    super(props);

    this.state = {
      // email: 'super@user.com',
      // password: '1913539350p'
      email: '',
      password: '',
      showPassword: false,
    }

    this.validator = new SimpleReactValidator();
  }

  // this method is only to trigger route guards , remove and use your own logic
  handleLogin = () => {
    if (this.validator.allValid()) {
      const {loginRequest} = this.props;
      const {email, password} = this.state;
      loginRequest({
        email: email,
        password: password
      });
    } else {
      this.validator.showMessages();
      // rerender to show messages for the first time
      // you can use the autoForceUpdate option to do this automatically`
      this.forceUpdate();
    }


    // History.push('/')
  }

  render() {
    const {email, password, showPassword} = this.state;
    return (
      <div className="d-flex justify-content-center align-items-center h-100">
        <div className="flex-column authFormBorder">
          <h1 className="text-center">CONTEMPORARY CONCIERGE</h1>
          <h3 className="text-center">YOUR PERSONAL CONCIERGE SERVICE</h3>
          <h2 className="text-center">Login</h2>
          <div>
            <TextField
              id="email-input"
              placeholder="Email"
              className="my-3 authInput"
              value={email}
              onChange={(event) => this.setState({email: event.target.value})}
            />
            {this.validator.message('email', email, 'required|email', { className: 'text-danger' })}
          </div>
          <div>
            <Input
              className="my-3 authInput"
              id="standard-adornment-password"
              placeholder="Password"
              type={showPassword ? 'text' : 'password'}
              value={password}
              onChange={event => this.setState({password: event.target.value})}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={(_) => this.setState({showPassword: !showPassword})}
                    onMouseDown={(event) => event.preventDefault()}
                  >
                    {showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              }
            />
            {this.validator.message('password', password, 'required|min:6|max:120')}
          </div>
          <h2 className="text-center primary-color mb-5 mt-2">Forgot password?</h2>
          <div className="authInput d-flex justify-content-center">
            <Button variant="contained" color="primary" size="large" className="flex-fill"
                    onClick={this.handleLogin}>Login</Button>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    lang : state.lang
  }
}

const mapDispatchToProps = dispatch => ({
  loginRequest: (data) => {
    dispatch(adminLogin(data));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
