import React, {Component} from 'react';
import InfiniteScroll from "react-infinite-scroll-component";
import API from "../../network/apis/APIExample";
import CustomerItem from "../../components/CustomerItem";
import MessageItem from "../../components/MessageItem";
import CircularProgress from "@material-ui/core/CircularProgress";

class Customer extends Component {

  // this method is only to trigger route guards , remove and use your own logic
  constructor(props) {
    super(props);

    this.state = {
      items: [],
      next_url: '',
      prev_url: '',
      total: 0,
    }
  }

  componentDidMount() {
    this.fetchData()
  }

  fetchData(next) {
    const {items} = this.state;
    if (next) {
      API.fetchMessages('?limit=10&offset=' + items.length).then((value) => {
        const {next, previous, results} = value.data;
        this.setState((prev) => ({items: [...prev.items, ...results], next_url: next, prev_urlL: previous}));
      })
    } else {
      API.fetchMessages('').then((value) =>{
        const {count, next, previous, results} = value.data;
        this.setState({items: results, total: count, next_url: next, prev_urlL: previous});
      })
    }
  }

  render(){
    const {items} = this.state;
    return (
      <div className="flex-fill container flex-column d-flex ">
        <div className="header row align-items-center justify-content-between">
          <h1>Customers</h1>
        </div>

        <div className="body flex-column justify-content-center" >
          <div className="flex-fill" id="scrollableDiv" style={{height: "calc(100vh - 194px - 2rem)"}}>
            <div className="row d-flex mx-0 align-items-center task-item-header">
              <div className="col-3">
                <h2 className='text-center'>User NAME</h2>
              </div>
              <div className="col-3">
                <h2 className='text-center'>SUBJECT</h2>
              </div>
              <div className="col-3">
                <h2 className='text-center'>MESSAGE</h2>
              </div>
              <div className="col-3">
                <h2 className='text-center'>TIME</h2>
              </div>
            </div>
            <InfiniteScroll
              scrollableTarget="scrollableDiv"
              dataLength={items.length} //This is important field to render the next data
              next={() => this.fetchData(true)}
              hasMore={
                <p style={{textAlign: 'center'}}>
                  <b>Loading...</b>
                </p>
              }
              loader={<div className="justify-content-center"><CircularProgress /> </div>}
              height={"calc(100vh - 230px)"}
              endMessage={
                <p style={{textAlign: 'center'}}>
                  <b>Yay! You have seen it all</b>
                </p>
              }
              refreshFunction={() => this.fetchData()}
              pullDownToRefresh
              pullDownToRefreshThreshold={50}
              pullDownToRefreshContent={
                <h3 style={{textAlign: 'center'}}>&#8595; Pull down to refresh</h3>
              }
              releaseToRefreshContent={
                <h3 style={{textAlign: 'center'}}>&#8593; Release to refresh</h3>
              }>
              {items.map(e => <MessageItem key={e.id} item={e}/>)}
            </InfiniteScroll>
          </div>
        </div>
      </div>
    )
  }
}

export default Customer;
