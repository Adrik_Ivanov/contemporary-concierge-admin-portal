import React from "react";
import { Router } from "react-router-dom";
import history from "../routes/History";
import Routes from "../routes/Routes";
import { MaterialSnackbar } from "../components/Snackbar/Snackbar";
import Loader from "../components/Loader/Loader";
import "./App.scss";
import { connect } from "react-redux";
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

class App extends React.Component {
  // App contains routes and also wrapped with snackbar and intl for localization
  render() {
    const { loading } = this.props;
    return (
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <div className="vh-100 w-100">
          {loading ? <Loader /> : null}
          <Router history={history}>
            <MaterialSnackbar />
            {/*<Navbar />*/}
            {Routes}
          </Router>
        </div>
      </MuiPickersUtilsProvider>
    );
  }
}

const mapStateToProps = ({ lang, loading }) => ({
  lang,
  loading
});

export default connect(mapStateToProps, null)(App);
