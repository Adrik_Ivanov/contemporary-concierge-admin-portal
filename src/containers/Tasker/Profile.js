import React, {Component} from 'react';
import {Btn} from '../../components/Controls/Button/Button';
import History, {navHistory} from '../../routes/History';
import IconButton from "@material-ui/core/IconButton";
import ArrowBackIos from "@material-ui/icons/ArrowBackIos";
import {Avatar} from "@material-ui/core";
import Star from "@material-ui/icons/Star";
import Place from "@material-ui/icons/Place";
import Assignment from "@material-ui/icons/Assignment";
import Button from "@material-ui/core/Button";
import CustomerRecentItem from "../../components/CustomerRecentItem";
import APIExample from "../../network/apis/APIExample";
import TaskDialog from "../../components/TaskDialog";
import Alert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";
import {connect} from "react-redux";
import {database} from "../../network/firebase/firestore";

class TaskerProfile extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            tasks: [],
            isDialogOpen: false,
            result: null,
        }
        this.db = database;
    }

    componentDidMount() {
        this.fetchData();
    }

    fetchData() {
        const tasker = this.props.location.state;
        APIExample.fetchRecentTasks(tasker.user.id).then((value) => this.setState({tasks: value.data}))
    }

    deleteTasker() {
        const tasker = this.props.location.state;
        APIExample.deleteTasker(tasker.user.id).then((e) => {
            this.setState({
                result: {
                    msg: "A Tasker was deleted successfully.",
                    success: true
                }
            });
            navHistory.replace('/tasker');
        }).catch(e => {
            this.setState({
                msg: "Something went wrong.",
                success: false
            });
        });
    }

    goToChatRoom(tasker) {
        const {user} = this.props;
        const roomId =
            "admin" + '-' + user.id + '-' + tasker?.user?.id;
        this.db
            .collection('chatRooms')
            .doc(roomId)
            .get()
            .then((value) => {
                if (value.exists) {
                    navHistory.push('chatting', value.data());
                } else {
                    const room = {
                        admin_id: user?.id,
                        admin_firstname: user?.first_name,
                        admin_lastname: user?.last_name,
                        admin_photo: user?.profile?.photo,
                        employee_id: tasker?.user?.id,
                        employee_firstname: tasker?.user?.first_name,
                        employee_lastname: tasker?.user?.last_name,
                        employee_photo: tasker?.photo,
                        enable: true,
                        sender_id: null,
                        last_message: null,
                        last_message_type: null,
                        last_updated: null,
                    };
                    this.db
                        .collection('chatRooms')
                        .doc(roomId)
                        .set(room)
                        .then((r) =>
                            navHistory.push('chatting', room)
                        );
                }
            });

    }

    render() {
        const tasker = this.props.location.state;
        const {tasks, isDialogOpen, result} = this.state;
        return (
            <div className="container row">
                {isDialogOpen && <TaskDialog id={tasker?.id} isOpen={isDialogOpen} onClose={(result) => {
                    if (result) {
                        this.setState({result});
                        this.fetchData();
                    }
                    this.setState({isDialogOpen: false});
                }}/>}
                <Snackbar open={!!result} autoHideDuration={3000} onClose={() => this.setState({successMsg: false})}>
                    <Alert onClose={() => this.setState({result: false})} severity={result?.success ? "success" : "error"}>
                        {result?.msg}
                    </Alert>
                </Snackbar>
                <div className="col-md-6 col-12 d-flex flex-column">
                    <div className="header align-items-center d-flex flex-row">
                        <IconButton aria-label="delete" color="primary" onClick={() => navHistory.replace('/tasker')}>
                            <ArrowBackIos className="dark-color"/>
                        </IconButton>
                        <h1 className="mb-0">{tasker?.user?.first_name} {tasker?.user?.last_name}</h1>
                    </div>
                    <div className="flex-column d-flex round-border py-5">
                        <div className="justify-content-center d-flex">
                            <Avatar className="profile-avatar" src={tasker?.photo}/>
                        </div>
                        <div className="justify-content-center d-flex mt-3">
                            <h1>{tasker?.user?.first_name} {tasker?.user?.last_name}</h1>
                        </div>
                        <div className="justify-content-center d-flex">
                            <h3>{tasker?.mobile_number}</h3>
                        </div>
                        <div className="justify-content-center d-flex">
                            <h3>{tasker?.user?.email}</h3>
                        </div>
                        <div className="justify-content-between d-flex my-4 px-4 mx-5">
                            <div className="row">
                                <Star className="tiny-icon warning-color mr-1"/>
                                <h3>5.0</h3>
                            </div>
                            <div className="row">
                                <Place className="tiny-icon primary-color mr-1"/>
                                <h3>{tasker?.category?.name}</h3>
                            </div>
                            <div className="row">
                                <Assignment className="tiny-icon primary-color mr-1"/>
                                <h3>{tasker?.count} tasks</h3>
                            </div>
                        </div>
                        <div className="justify-content-center d-flex">
                            <Button onClick={() => this.setState({isDialogOpen: true})} className="width-148 mx-3" size="large" color="primary" variant="contained">
                                Assign Task
                            </Button>
                            <Button onClick={() => this.goToChatRoom(tasker)}
                                    className="width-148 mx-3 primary-color" size="large" color="primary"
                                    variant="outlined">
                                Message
                            </Button>
                            <Button onClick={() => this.deleteTasker()} className="width-148 mx-3 primary-color" size="large" color="secondary"
                                    variant="outlined">
                                Delete
                            </Button>
                        </div>
                    </div>
                </div>
                <div className="col-md-6 col-12">
                    <div className="header align-items-center d-flex">
                        <h1 className="mb-0">Recent tasks</h1>
                    </div>
                    <div className="flex-column d-flex round-border px-3">
                        <div className="row d-flex mx-0 align-items-center task-item-header">
                            <div className="col-6">
                                <h2 className='text-center'>CLIENT</h2>
                            </div>
                            <div className="col-6">
                                <h2 className='text-center'>ADDRESS</h2>
                            </div>
                        </div>
                        {tasks.map(e => <CustomerRecentItem key={e.id} item={e}/>)}
                        {!tasks.length && <div className="my-3">
                            <h2 className="text-center">Nothing</h2>
                        </div>}
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.auth.user
    }
}

export default connect(mapStateToProps, null)(TaskerProfile);
