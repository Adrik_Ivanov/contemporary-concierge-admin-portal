import React, {Component} from 'react';
import Button from "@material-ui/core/Button";
import {navHistory} from "../../routes/History";
import NewApartment from "../../components/NewApartment";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import SwipeableViews from "react-swipeable-views";
import TaskerTab from "../../components/TaskerTab";

class Tasker extends Component {

  // this method is only to trigger route guards , remove and use your own logic
  constructor(props) {
    super(props);

    this.state = {
      selectedTab: 0,
      isDialogOpen: false,
      successMsg: false,
      selectedCustomer: null,
    }
  }

  componentDidMount() {
  }

  render(){
    const {selectedTab, isDialogOpen, successMsg, selectedCustomer} = this.state;
    return (
      <div className="flex-fill container flex-column d-flex ">
        {isDialogOpen && <NewApartment data={selectedCustomer} isOpen={isDialogOpen} onClose={(value) => {
          if (value) {
            this.refresh();
            this.setState({successMsg: value});
          }
          this.setState({isDialogOpen: false, selectedCustomer: null});
        }}/>}
        <Snackbar open={successMsg} autoHideDuration={3000} onClose={() => this.setState({successMsg: false})}>
          <Alert onClose={() => this.setState({successMsg: false})} severity={successMsg?.success ? "success" : "error"}>
            {successMsg?.msg}
          </Alert>
        </Snackbar>
        <div className="header row align-items-center justify-content-between">
          <h1>Appointments</h1>
          <Tabs
            value={selectedTab}
            onChange={(event, value) => this.setState({selectedTab: value})}
            indicatorColor="primary"
            textColor="primary"
            centered
          >
            <Tab className="small-tab" label="All"/>
            <Tab className="small-tab" label="Pending" />
          </Tabs>
        </div>
        <div className="body flex-column justify-content-center flex-fill d-flex">
          <div className="row d-flex mx-0 align-items-center task-item-header">
            <div className="col-3">
              <h2 className='text-center'>TASKER</h2>
            </div>
            <div className="col-3">
              <h2 className='text-center'>CATEGORY</h2>
            </div>
            <div className="col-3">
              <h2 className='text-center'>CONTACT NO</h2>
            </div>
            <div className="col-3">
              <h2 className='text-center'>EMAIL ID</h2>
            </div>
          </div>
          <SwipeableViews
            style={{flex: 1}}
            index={selectedTab}
            onChangeIndex={this.handleChangeIndex}>
            <TaskerTab value={selectedTab} index={0} query={'?'}/>
            <TaskerTab value={selectedTab} index={1} query={'?status=requested'}/>
          </SwipeableViews>
          <div className="w-100 justify-content-center align-items-center d-flex">
            <Button variant="contained" color="primary" size="large" className="w-100 my-3"
                    onClick={() => navHistory.push('new_tasker')}>Add tasker</Button>
          </div>
        </div>
      </div>
    )
  }
}

export default Tasker;
