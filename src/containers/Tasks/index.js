import React, {Component} from 'react';
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import SwipeableViews from "react-swipeable-views";
import TabPanel from "../../components/TabPanel";

class Tasks extends Component {

  // this method is only to trigger route guards , remove and use your own logic
  constructor(props) {
    super(props);

    this.state = {
      selectedTab: 0,
    }
  }

  handleChangeTab = (event, newValue) => {
    this.setState({selectedTab: newValue});
  }

  handleChangeIndex = (index) => {
    this.setState({selectedTab: index});
  };

  render(){
    const {selectedTab} = this.state;
    return(
      <div className="flex-fill container flex-column d-flex">
        <div className="header row align-items-center justify-content-between">
          <h1>Appointments</h1>
          <Tabs
            value={selectedTab}
            onChange={this.handleChangeTab}
            indicatorColor="primary"
            textColor="primary"
            centered
          >
            <Tab className="small-tab" label="All"/>
            <Tab className="small-tab" label="Unassigned" />
          </Tabs>
        </div>

        <div className="body flex-row justify-content-center flex-fill d-flex">
          <SwipeableViews
            style={{flex: 1}}
            index={selectedTab}
            onChangeIndex={this.handleChangeIndex}>
            <TabPanel value={selectedTab} index={0} query={'?'}/>
            <TabPanel value={selectedTab} index={1} query={'?status=unassigned'}/>
          </SwipeableViews>

        </div>
      </div>
    )
  }
}


export default Tasks;
