import React, {Component, forwardRef} from 'react';
import {navHistory} from '../../routes/History';
import TabPanel from '../../components/TabPanel'
import {connect} from "react-redux";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import GoogleMapReact from 'google-map-react';
import APIExample from "../../network/apis/APIExample";
import LocationOnIcon from '@material-ui/icons/LocationOn';

const GEOPin = forwardRef((props, ref) =>
    <LocationOnIcon ref={ref} {...props} />);

class MapDashboard extends Component {

  constructor(props) {
    super(props);

    this.state = {
      selectedCategory: -1,
      result: null,
      positions: [],
    }
  }

  componentDidMount() {
    this.fetchPositions()
  }

  fetchPositions() {
    APIExample.fetchPositions().then(e => {
      console.log(e);
      this.setState({positions: e.data});
    });
  }

  render() {
    const {selectedCategory, positions} = this.state;
    const {apartments} = this.props;
    let category = '';
    if (selectedCategory === 0) {
      category = '&apartment=0';
    } else if (selectedCategory?.id) {
      category = '&apartment=' + selectedCategory?.id;
    }
    return(
      <div className="flex-fill container">
        <div className="row align-items-center justify-content-between mx-3" style={{height: '10vh'}}>
          <h1>Map View of Taskers</h1>
        </div>
        <div className="flex-row justify-content-center">
          <div className="row mx-3" style={{height: '35vh'}}>
            <GoogleMapReact
              bootstrapURLKeys={{ key: 'AIzaSyBN2y8G8Aq2-CcbcarL3QbuqBTSjMtFIsM' }}
              defaultCenter={{
                lat: 29.7538661,
                lng: -95.3859492
              }}
              defaultZoom={11}>
              {positions.map((e, index) => <GEOPin
                  key={index}
                  lat={e.latitude}
                  lng={e.longitude}
                  style={{color: '#67ADBF', fontSize: 32, width: 32, height: 32}}
              />)}
            </GoogleMapReact>
          </div>
        </div>
        <div className="row align-items-center justify-content-between mx-3" style={{height: '10vh'}}>
          <h1>In progress tasks</h1>
          <Select
            className="width-148 overflow-hidden"
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={selectedCategory}
            onChange={(event) => this.setState({selectedCategory: event.target.value})}
          >
            <MenuItem value={0}>All</MenuItem>
            {apartments.map(e => <MenuItem key={e.id} value={e}>{e?.name}</MenuItem>)}
          </Select>
        </div>
        <div className="flex-row justify-content-center">
          <div className="row d-flex mx-0 align-items-center border-bottom border-top" style={{height: 50}}>
            <div className="col-2">
              <h2 className='text-center'>CUSTOMER NAME</h2>
            </div>
            <div className="col-2">
              <h2 className='text-center'>TASK</h2>
            </div>
            <div className="col-2">
              <h2 className='text-center'>Time</h2>
            </div>
            <div className="col-2">
              <h2 className='text-center'>SERVICE REQUEST</h2>
            </div>
            <div className="col-2">
              <h2 className='text-center'>ADDRESS</h2>
            </div>
            <div className="col-2">
              <h2 className='text-center'>TASKER</h2>
            </div>
          </div>
          <div style={{height: 'calc(35vh - 50px)'}}>
            <TabPanel height="calc(35vh - 50px)" value={0} index={0} isMapItem query={'?status=in_progress' + (category ?? '')}/>
            {/*<TabPanel value={0} index={0} query={'?status=in_progress' + (category ?? '')}/>*/}
          </div>
        </div>
        <div className="w-100 justify-content-center align-items-center d-flex" style={{height: '10vh'}}>
          <Button variant="contained" color="primary" size="large" className="w-100 my-3"
                  onClick={() => navHistory.push('/dashboard')}>View all</Button>
        </div>
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    apartments : state.auth.apartments
  }
}

export default connect(mapStateToProps, null)(MapDashboard);
