import React from "react";
import {Avatar} from "@material-ui/core";
import {navHistory} from "../../routes/History";

const AdminItem = ({item}) => {
  return (
    <div key={item?.id} className="row d-flex mx-0 align-items-center task-item" onClick={() => {}}>
      <div className="col-3 flex-row d-flex align-items-center">
        <Avatar src={item?.profile?.photo} className="list-avatar"/>
        <div className="ml-3">
          <h2 className='text-center'>{item?.first_name} {item?.last_name}</h2>
        </div>
      </div>
      <div className="col-3">
        <h2 className='text-center'>{item?.profile?.address ?? "UnKnown"}</h2>
      </div>
      <div className="col-3">
        <h2 className='text-center'>{item?.profile?.phone_number ?? "UnKnown"}</h2>
      </div>
      <div className="col-3">
        <h2 className='text-center'>{item?.email}</h2>
      </div>
    </div>
  );
};

export default AdminItem;
