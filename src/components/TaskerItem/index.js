import React from "react";
import {Avatar} from "@material-ui/core";
import {navHistory} from "../../routes/History";

const TaskerItem = ({item}) => {
  return (
    <div key={item?.id} className="row d-flex mx-0 align-items-center task-item" onClick={() => navHistory.push('/tasker_profile', item)}>
      <div className="col-3 flex-row d-flex align-items-center">
        <Avatar src={item?.photo} className="list-avatar"/>
        <div className="ml-3">
          <h2 className='text-center'>{item?.user?.first_name} {item?.user?.last_name}</h2>
        </div>
      </div>
      <div className="col-3">
        <h2 className='text-center'>{item?.category?.id ? item?.category?.name : 'Garbage Removal'}</h2>
      </div>
      <div className="col-3">
        <h2 className='text-center'>{item?.mobile_number}</h2>
      </div>
      <div className="col-3">
        <h2 className='text-center'>{item?.user?.email}</h2>
      </div>
    </div>
  );
};

export default TaskerItem;
