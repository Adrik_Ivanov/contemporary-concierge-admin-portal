import React, {useState} from "react";
import Moment from "moment";
import {Avatar} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import TaskerDialog from "../TaskerDialog";

const TaskItem = ({item, refresh, isMapItem=false}) => {
  const [isDialogOpen, setIsDialogOpen] = useState(false);

  return (
    <div key={item?.id} className="row d-flex mx-0 align-items-center task-item">
      {isDialogOpen && <TaskerDialog id={item?.id} isOpen={isDialogOpen} onClose={(value) => {
        if (value) {
          refresh(value);
        }
        setIsDialogOpen(false);
      }}/>}
      <div className="col-2 flex-row d-flex align-items-center">
        <Avatar src={item?.customer?.photo} className="list-avatar"/>
        <div className="ml-2">
          <h2 className='font-weight-normal text-center'>{item?.customer?.first_name} {item?.customer?.last_name}</h2>
          <h3 className='text-dark'>{item?.phone_number}</h3>
        </div>
      </div>
      <div className="col-2">
        <h2 className='font-weight-normal text-center'>{item?.category?.id ? item?.category?.name : 'Garbage Removal'}</h2>
      </div>
      <div className="col-2">
        <h2 className='font-weight-normal text-center'>{Moment(item.task_date).format('MMM DD, YYYY')} {item?.task_time_from.substring(0, 5)}</h2>
      </div>
        <div className="col-2">
            <h2 className='font-weight-normal text-center'>{item?.description}</h2>
        </div>
        <div className="col-2">
            <h2 className='font-weight-normal text-center'>{item?.community_name?.name}</h2>
        </div>
      <div className="col-2 d-flex justify-content-center">
        {item?.tasker && <div className=" flex-row d-flex align-items-center">
          <Avatar src={item?.tasker?.photo} className="list-avatar"/>
          <div className="ml-2">
            <h2 className='font-weight-normal text-center'>{item?.tasker?.first_name} {item?.tasker?.last_name}</h2>
            <h3 className='text-dark'>{item?.tasker?.phone_number}</h3>
          </div>
        </div>}
        {!item?.tasker && <Button onClick={() => setIsDialogOpen(true)} size="small" color="primary" variant="contained">Assign Tasker</Button>}
      </div>
    </div>
  );
};

export default TaskItem;
