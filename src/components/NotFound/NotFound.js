import React from "react";
import History from "../../routes/History";

const NotFound = () => {
  History.push('/');
  return (
    <React.Fragment>
      <div className="text-center">
        <h1 className="my-5 pt-5">Sorry we can’t find this page</h1>
      </div>
    </React.Fragment>
  );
};
export default NotFound;
