import React, {Component} from "react";
import InfiniteScroll from 'react-infinite-scroll-component';
import API from '../../network/apis/APIExample'
import TaskerItem from "../TaskerItem";
import CircularProgress from "@material-ui/core/CircularProgress";

export default class TaskerTab extends Component{

  constructor(props) {
    super(props);

    this.state = {
      items: [],
      next_url: '',
      prev_url: '',
      total: 0,
      page: 0,
    }
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData(next) {
    const {query} = this.props;
    const {items} = this.state;
    if (next) {
      API.fetchTasker(query + 'limit=10&offset=' + items.length).then((value) =>{
        const {count, next, previous, results} = value.data;
        this.setState((prev) => ({items: [...prev.items, ...results], next_url: next, prev_urlL: previous}));
      })
    } else {
      API.fetchTasker(query).then((value) =>{
        const {count, next, previous, results} = value.data;
        this.setState({items: results, total: count, next_url: next, prev_urlL: previous});
      })
    }
  }

  render() {
    const {items, total} = this.state;
    const {value, index} = this.props
    return (
      <div
        className="d-flex flex-column flex-fill"
        role="tabpanel"
        hidden={value !== index}
        id={`full-width-tabpanel-${index}`}
        aria-labelledby={`full-width-tab-${index}`}
      >

        <div id="scrollableDiv" className="flex-fill">
          {value === index && (
            <InfiniteScroll
              scrollableTarget="scrollableDiv"
              dataLength={items.length} //This is important field to render the next data
              next={() => this.fetchData(true)}
              hasMore={total > items.length}
              loader={
                <div className="justify-content-center"><CircularProgress /> </div>
              }
              height={"calc(100vh - 284px - 2rem)"}
              style={{overflowX: "hidden"}}
              endMessage={
                <p style={{textAlign: 'center'}}>
                  <b>Yay! You have seen it all</b>
                </p>
              }
              refreshFunction={() => this.fetchData()}
              pullDownToRefresh
              pullDownToRefreshThreshold={50}
              pullDownToRefreshContent={
                <h3 style={{textAlign: 'center'}}>&#8595; Pull down to refresh</h3>
              }
              releaseToRefreshContent={
                <h3 style={{textAlign: 'center'}}>&#8593; Release to refresh</h3>
              }>
              {items.map(e => <TaskerItem key={e.id} item={e}/>)}
            </InfiniteScroll>
          )}
        </div>
      </div>
    );
  }
}
