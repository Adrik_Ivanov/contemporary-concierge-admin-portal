import React from "react";
import {Avatar} from "@material-ui/core";
import {navHistory} from "../../routes/History";
import Moment from "moment";

const MessageItem = ({item}) => {
  return (
    <div key={item?.id} className="row d-flex mx-0 align-items-center task-item" onClick={() => navHistory.push('/customer_profile', item)}>
      <div className="col-3 flex-row d-flex align-items-center">
        {/*<Avatar src={item?.photo} className="list-avatar"/>*/}
        {/*<div className="ml-3">*/}
          <h2 className='text-center'>{item?.user?.first_name} {item?.user?.last_name}</h2>
        {/*</div>*/}
      </div>
      <div className="col-3">
        <h2 className='text-center'>{item?.subject}</h2>
      </div>
      <div className="col-3">
        <h2 className='text-center'>{item?.message}</h2>
      </div>
      <div className="col-3">
        <h2 className='text-center'>{Moment(item?.created_at ?? '').format("MMM DD, yyyy")}</h2>
      </div>
    </div>
  );
};

export default MessageItem;
