import React from "react";
import {Avatar} from "@material-ui/core";
import LinearProgress from '@material-ui/core/LinearProgress';

const NavUser = ({user}) => {
  return (
    <div className="nav-user-container justify-content-center flex-column">

      <div className="row d-flex align-items-center">
        <div> <Avatar src={user?.profile?.photo ?? ''}/></div>
        <div className="flex-fill ml-2">
          <h1 className="mb-0 text-white">{user?.first_name ?? ''} {user?.last_name}</h1>
          {/*<h2 className="text-white">Account level 14</h2>*/}
        </div>
      </div>
      {/*<div className="row w-100 justify-content-between mt-5">*/}
      {/*  <h3>765 points</h3>*/}
      {/*  <h3>1000</h3>*/}
      {/*</div>*/}
      {/*<LinearProgress className="w-100 " variant="determinate" value={76} />*/}
    </div>
  );
};

export default NavUser;
