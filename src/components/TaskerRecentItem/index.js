import React from "react";
import {Avatar} from "@material-ui/core";

const TaskerRecentItem = ({item}) => {
  return (
    <div key={item?.id} className="row d-flex mx-0 align-items-center task-item">
      <div className="col-6 flex-row d-flex align-items-center">
        <Avatar src={item?.photo} className="list-avatar"/>
        <div className="ml-3">
          {item?.tasker && <h2 className='text-center'>{item?.tasker?.first_name} {item?.tasker?.last_name}</h2>}
          {!item?.tasker && <h2 className='text-center' style={{color: 'red'}}>Unassigned</h2>}
        </div>
      </div>
      <div className="col-6">
        <h2 className='text-center'>{item?.category?.id ? item?.category?.name : 'Garbage Removal'}</h2>
      </div>
    </div>
  );
};

export default TaskerRecentItem;
