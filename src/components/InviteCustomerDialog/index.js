import React, {useEffect, useRef, useState} from "react";

import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import APIExample from "../../network/apis/APIExample";
import Dialog from "@material-ui/core/Dialog";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import { ReactSVG } from 'react-svg'

const InviteCustomerDialog = ({isOpen, onClose}) => {
    const [file, setFile] = useState();
    const inputFile = useRef(null)

    const uploadFile = () => {
        if (file) {
            const form = new FormData();
            form.append('file', file);
            APIExample.uploadCustomerExcel(form).then((_) => onClose({
                success: true,
                msg: 'The invitations are completed successfully.'
            })).catch((e) => {
                if(e.status === 500) {
                    onClose({
                        success: false,
                        msg: 'there are some issues in file format.'
                    })
                } else {
                    onClose({
                        success: false,
                        msg: 'Something went wrong.'
                    })
                }
            });
        }
    }

    const onOpenFileDialog = (event) => {
        event.stopPropagation();
        event.preventDefault();
        const file = event.target.files[0];
        console.log(file);
        setFile(file);
    }

    return (
        <Dialog onClose={() => onClose()} aria-labelledby="simple-dialog-title" open={isOpen}  maxWidth="sm">
            <div className="w-100 justify-content-end align-items-end d-flex">
                <IconButton aria-label="close" onClick={(event) => onClose(false)}>
                    <CloseIcon />
                </IconButton>
            </div>
            <div className='vw-100 overflow-hidden'/>
            <DialogTitle id="simple-dialog-title">Invite customer</DialogTitle>
            <div className='align-items-center d-flex row mx-5 border-bottom align-items-center'>
                <div className="mt-4 d-flex flex-row" >
                    <input id="myInput"
                           type="file"
                           ref={inputFile}
                           style={{display: 'none'}}
                           onChange={(event) => onOpenFileDialog(event)}
                           accept=".xls, .xlsx, .xlsm, .csv"
                    />
                    <ReactSVG src="excel.svg" />
                    <Button className="ml-4 p-0" onClick={() => inputFile.current.click()}>
                        <h2 className="primary-color font-weight-normal">Upload Excel</h2>
                    </Button>
                    {file && <h3 className="mb-0 mt-1 ml-3">{file.name}</h3>}
                </div>
                <div className="w-100 justify-content-center align-items-center d-flex">
                    <Button variant="contained" color="primary" size="large" className="w-100 my-5"
                            onClick={() => uploadFile()}>Invite</Button>
                </div>
            </div>
        </Dialog>
    );
}

export default InviteCustomerDialog;
