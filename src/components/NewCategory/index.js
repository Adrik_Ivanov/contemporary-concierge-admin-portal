import React, {useEffect, useRef, useState} from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {navHistory} from "../../routes/History";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from '@material-ui/icons/Close';
import APIExample from "../../network/apis/APIExample";
import SimpleReactValidator from 'simple-react-validator';

const NewCategory = ({data, isOpen, onClose}) => {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const simpleValidator = useRef(new SimpleReactValidator())
  useEffect(()=>{
    if (data) {
      setDescription(data ? data.description : '');
      setName(data ? data.name : '');
    }
  }, [data])
  const handleClose = () => {
    onClose();
  };

  const onSubmit = () => {

    if (simpleValidator.current.allValid()) {
      if (data) {
        APIExample.updateCategory(data.id, {
          name: name,
          description: description
        }).then((e) => onClose({
          msg: "A Category was updated successfully.",
          success: true
        })).catch(_ => onClose({
          msg: "Category updating was failed",
          success: true
        }));
      } else {
        APIExample.createCategory({
          name: name,
          description: description
        }).then((e) => onClose({
          msg: "A Category was created successfully.",
          success: true
        })).catch(_ => onClose({
          msg: "Category creation was failed",
          success: true
        }));
      }
    } else {
      simpleValidator.current.showMessages();
    }
  };

  const onDelete = () => {
    APIExample.deleteCategory(data.id).then((e) => onClose({
      msg: "A Category was deleted successfully.",
      success: true
    })).catch(_ => onClose({
      msg: "Category deletion was failed",
      success: true
    }));
  }
  return (
    <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={isOpen}>
      <div className="w-100 justify-content-end align-items-end d-flex">
        <IconButton aria-label="close" onClick={(event) => onClose(false)}>
          <CloseIcon />
        </IconButton>
      </div>
      <DialogTitle id="simple-dialog-title">{data ? 'Update' : 'Add'} Category</DialogTitle>
      <div className='align-items-center d-flex flex-column mx-5'>
        <TextField
          id="name-input"
          placeholder="Category name"
          className="my-3 authInput"
          value={name ?? ''}
          onChange={(event) => setName(event.target.value)}
          onBlur={simpleValidator.current.message('name', name, 'required')}
        />
        {simpleValidator.current.message('name', name, 'required', { className: 'text-danger' })}
        <TextField
          id="description-input"
          placeholder="Category description"
          className="my-3 authInput"
          value={description ?? ''}
          onChange={(event) => setDescription(event.target.value)}
          onBlur={simpleValidator.current.message('description', description, 'required')}
        />
        {simpleValidator.current.message('description', description, 'required', { className: 'text-danger' })}
      </div>
      <div className="w-auto justify-content-center align-items-center d-flex mb-4 mx-5">
        <Button variant="contained" color="primary" size="large" className="w-100 my-3"
                onClick={() => onSubmit()}>Save Category</Button>
        {!!data && <Button variant="contained" color="primary" size="large" className="w-100 my-3 ml-3"
                 onClick={() => onDelete()}>Delete Category</Button>}
      </div>
    </Dialog>
  );
};
export default NewCategory;
