import React from "react";

const ListItem = ({title, tailing, onTap}) => {
  return (
    <div className="list-item justify-content-between flex-row align-items-center d-flex" onClick={() => onTap()}>
      <h1 className="list-item-text">{title}</h1>
      <p className="list-item-tailing">{tailing}</p>
    </div>
  );
};
export default ListItem;
