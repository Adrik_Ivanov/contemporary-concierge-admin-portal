import React, {useEffect, useState} from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from '@material-ui/icons/Close';
import APIExample from "../../network/apis/APIExample";
import {Avatar} from "@material-ui/core";
import CircularProgress from "@material-ui/core/CircularProgress";
import InfiniteScroll from "react-infinite-scroll-component";

const TaskDialog = ({id, isOpen, onClose}) => {
  const [unAssignedTasks, setUnAssignedTasks] = useState([]);
  const [total, setTotal] = useState(0);

  useEffect(()=>{
    fetchData();
  }, []);

  const fetchData = (next) => {
    if (next) {
      APIExample.fetchTasks('?status=unassigned?limit=10&offset=' + unAssignedTasks.length).then((value) => {
        setUnAssignedTasks([...unAssignedTasks, ...value.data.results]);
      });
    } else {
      APIExample.fetchTasks('?status=unassigned').then((value) => {
        setUnAssignedTasks(value.data.results);
        setTotal(value.data.count);
      });
    }
  }

  const handleClose = () => {
    onClose();
  };

  const onAssign = (item) => {
    APIExample.assignTask('tasker=' + id + '&task=' + item?.id).then((e) => onClose({
      msg: "A Task was assigned successfully.",
      success: true
    })).catch(_ => onClose({
      msg: "Task assigning was failed",
      success: false
    }));
  }

  const renderItem = (item) => {
    return (
        <div key={item.id} className='align-items-center d-flex row mx-5 border-bottom'>
          <div className="col-4 flex-row d-flex align-items-center">
            <Avatar src={item?.customer?.photo} className="list-avatar"/>
            <div className="ml-3">
              <h2 className='font-weight-normal text-center'>{item?.customer?.first_name} {item?.customer?.last_name}</h2>
              <h3 className='text-dark'>{item?.phone_number}</h3>
            </div>
          </div>
          <div className="col-4">
            <h2 className='font-weight-normal text-center'>{item?.category?.id ? item?.category?.name : 'Garbage Removal'}</h2>
          </div>
          <div className="col-4">
            <Button variant="contained" color="primary" size="small" className="w-100 my-3"
                    onClick={() => onAssign(item)}>Assign</Button>
          </div>
        </div>
    );
  }

  return (
      <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={isOpen}  maxWidth="md">
        <div className="w-100 justify-content-end align-items-end d-flex">
          <IconButton aria-label="close" onClick={(event) => onClose(false)}>
            <CloseIcon />
          </IconButton>
        </div>
        <div className='vw-100 overflow-hidden'/>
        <DialogTitle id="simple-dialog-title">Select Task</DialogTitle>
        <div className='align-items-center d-flex row mx-5 border-bottom'>
          <div className="col-4">
            <h2 className="dark-color text-center">CUSTOMER</h2>
          </div>
          <div className="col-4">
            <h2 className="dark-color text-center">TASK</h2>
          </div>
          <div className="col-4" />
        </div>
        <div className="max-height-75vh overflow-auto">
          <InfiniteScroll
              scrollableTarget="scrollableDiv"
              dataLength={unAssignedTasks.length} //This is important field to render the next data
              next={() => fetchData(true)}
              hasMore={total > unAssignedTasks.length}
              loader={
                <div className="w-100 justify-content-center d-flex mt-5"><CircularProgress /></div>
              }
              height={"calc(100vh - 284px - 2rem)"}
              endMessage={
                <p style={{textAlign: 'center'}}>
                  <b>Yay! You have seen it all</b>
                </p>
              }
              refreshFunction={() => fetchData(false)}
              pullDownToRefresh
              pullDownToRefreshThreshold={50}
              pullDownToRefreshContent={
                <h3 style={{textAlign: 'center'}}>&#8595; Pull down to refresh</h3>
              }
              releaseToRefreshContent={
                <h3 style={{textAlign: 'center'}}>&#8593; Release to refresh</h3>
              }>
            {unAssignedTasks.map(e => renderItem(e))}
          </InfiniteScroll>
        </div>
      </Dialog>
  );
};

export default TaskDialog;
