import React, {Component} from "react";
import InfiniteScroll from 'react-infinite-scroll-component';
import API from '../../network/apis/APIExample'
import TaskItem from "../TaskItem";
import Alert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";
import CircularProgress from "@material-ui/core/CircularProgress";

export default class TabPanel extends Component{

  constructor(props) {
    super(props);

    this.state = {
      items: [],
      next_url: '',
      prev_url: '',
      total: 0,
      page: 0,
    }
  }

  componentDidMount() {
    this.fetchData();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.query !== this.props.query) {
      this.fetchData();
    }
  }

  fetchData(next) {
    const {query} = this.props;
    const {items} = this.state;
    if (next) {
      API.fetchTasks(query + 'limit=10&offset=' + items.length).then((value) =>{
        const {count, next, previous, results} = value.data;
        this.setState((prev) => ({items: [...prev.items, ...results], next_url: next, prev_url: previous}));
      })
    } else {
      API.fetchTasks(query).then((value) =>{
        const {count, next, previous, results} = value.data;
        this.setState({items: results, total: count, next_url: next, prev_url: previous});
      })
    }
  }

  render() {
    const {items, total} = this.state;
    const {value, index, refresh, height, isMapItem} = this.props;
    return (
      <div
        className="d-flex flex-column flex-fill"
        role="tabpanel"
        hidden={value !== index}
        id={`full-width-tabpanel-${index}`}
        aria-labelledby={`full-width-tab-${index}`}
      >
        <div id="scrollableDiv" className="flex-fill">
          {value === index && (
            <InfiniteScroll
              scrollableTarget="scrollableDiv"
              dataLength={items.length} //This is important field to render the next data
              next={() => this.fetchData(true)}
              hasMore={total > items.length}
              loader={
                <div className="justify-content-center"><CircularProgress /> </div>
              }
              height={height ?? "calc(100vh - 284px - 2rem)"}
              endMessage={
                <p style={{textAlign: 'center'}}>
                  <b>Yay! You have seen it all</b>
                </p>
              }
              refreshFunction={() => this.fetchData()}
              pullDownToRefresh
              pullDownToRefreshThreshold={50}
              pullDownToRefreshContent={
                <h3 style={{textAlign: 'center'}}>&#8595; Pull down to refresh</h3>
              }
              releaseToRefreshContent={
                <h3 style={{textAlign: 'center'}}>&#8593; Release to refresh</h3>
              }>
              {items.map((e, index) => <TaskItem key={e.id + '-' + index} item={e} isMapItem={isMapItem ?? false} refresh={(value) => {
                refresh(value);
                this.fetchData();
              }}/>)}
            </InfiniteScroll>
          )}
        </div>
      </div>
    );
  }
}
