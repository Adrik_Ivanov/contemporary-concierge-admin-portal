import React from "react";
import {Avatar} from "@material-ui/core";

const CustomerRecentItem = ({item}) => {
  return (
    <div key={item?.id} className="row d-flex mx-0 align-items-center task-item">
      <div className="col-6 flex-row d-flex align-items-center">
        <Avatar src={item?.photo} className="list-avatar"/>
        <div className="ml-3">
          <h2 className='text-center'>{item?.customer?.first_name} {item?.customer?.last_name}</h2>
        </div>
      </div>
      <div className="col-6">
        <h2 className='text-center'>{item?.community_name?.name}</h2>
      </div>
    </div>
  );
};

export default CustomerRecentItem;
