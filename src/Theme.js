import React, { useState, useEffect } from "react";
import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";
import rtl from "jss-rtl";
import { create } from "jss";
import { StylesProvider, jssPreset } from "@material-ui/styles";
import { useSelector } from "react-redux";
import App from "./containers/App";

function ThemeApp() {
  const jss = create({ plugins: [...jssPreset().plugins, rtl()] });
  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#67ADBF"
      },
      secondary: {
        main: "#ac4556"
      }
    },
    overrides: {
      MuiInputBase: {
        root: {
          color: '#74787B'
        }
      },
      MuiButton: {
        label: {
          fontFamily: 'Source Sans Pro',
          letterSpacing: -0.165
        },
        sizeLarge: {
          maxWidth: 328,
          height: 54,
        },
        sizeSmall: {
          width: 156,
          height: 44,
        },
        textSizeLarge: 18,
        textSizeSmall: 15,
        containedPrimary: {
          color: 'white'
        }
      },
      MuiDivider: {
        root: {
          backgroundColor: 'rgba(255, 255, 255, 0.28)'
        }
      },
      MuiListItem: {
        gutters: {
          paddingLeft: 38,
        }
      },
      MuiAvatar: {
        root: {
          width: 64,
          height: 64,
        }
      },
      MuiLinearProgress: {
        root: {
          height: 8,
          borderRadius: 4
        },
        colorPrimary: {
          backgroundColor: '#5491A0',
        },
        barColorPrimary: {
          backgroundColor: 'white'
        },
      },
      MuiTab: {
        root: {
          textTransform: "none",
          fontSize: 24,
          fontFamily: 'Oswald',
          fontWeight: 'normal',
          letterSpacing: -0.165,
        }
      },
      MuiTypography: {
        h6: {
          fontFamily: 'Oswald',
          fontWeight: "normal",
          letterSpacing: -0.165,
          color: '#545759'
        }
      },
      MuiDialogTitle: {
        root: {
          paddingTop: 0,
          paddingLeft: '3rem'
        }
      },
      MuiSwitch: {
        root: {
          display: "flex",
          width: 40,
          height: 24,
          padding: 0,
          borderRadius: 20,
        },
        switchBase: {
          padding: 4,
          color: "#CAC7C7",
          '&$checked': {
            transform: 'translateX(16px)',
            color: "white",
            '& + $track': {
              opacity: 1,
              backgroundColor: "#67ADBF",
              borderColor: "#67ADBF",
            },
          }
        },
        colorPrimary: {
          color: 'white',
          '&$checked': {
            color: "white",
          }
        },
        thumb: {
          width: 16,
          height: 16,
          boxShadow: 'none',
        },
        track: {
          borderRadius: 16 / 2,
          opacity: 1,
          backgroundColor: '#CAC7C7'
        },
        checked: {
          color: 'white'
        }
      },
    },
  });
  return (
    <StylesProvider jss={jss}>
      <ThemeProvider theme={theme}>
        <App />
      </ThemeProvider>
    </StylesProvider>
  );
}

export default ThemeApp;
