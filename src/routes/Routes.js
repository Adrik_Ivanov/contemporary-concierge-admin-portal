import React, { Suspense } from "react";
import { Router, Switch } from "react-router-dom";
import history, {navHistory} from "./History";
import * as LazyComponent from "../utils/LazyLoaded";
import Loader from "../components/Loader/Loader";
import PrivateRoute from "../utils/PrivateRoute";

const Routes = (
  <Suspense fallback={<Loader />}>
    <Router history={history}>
      <Switch>
        {/* For private routes */}
        <PrivateRoute component={LazyComponent.Home} path="/" exact />
        {/*<PrivateRoute component={LazyComponent.Message} path="/message" exact />*/}
        {/* Public routes that doesn't need any auth */}
        <LazyComponent.Login path="/login" exact />
        {/*<LazyComponent.MapDashboard path="/test" exact />*/}
        <LazyComponent.NotFound path="**" title="This page doesn't exist..." exact />
      </Switch>
    </Router>
  </Suspense>
);

export const drawerRoutes = (
  <Suspense fallback={<Loader />}>
    <Router history={navHistory}>
      <Switch>
        {/* For private routes */}
        <PrivateRoute component={LazyComponent.Dashboard} path="/dashboard" exact />
        <PrivateRoute component={LazyComponent.Administrator} path="/administrator" exact />
        <PrivateRoute component={LazyComponent.NewAdministrator} path="/new_administrator" exact />
        <PrivateRoute component={LazyComponent.Category} path="/category" exact />
        <PrivateRoute component={LazyComponent.Customer} path="/customer" exact />
        <PrivateRoute component={LazyComponent.InviteCustomer} path="/invite-customer" exact />
        <PrivateRoute component={LazyComponent.CustomerProfile} path="/customer_profile" exact />
        <PrivateRoute component={LazyComponent.Message} path="/message" exact />
        <PrivateRoute component={LazyComponent.Setting} path="/setting" exact />
        <PrivateRoute component={LazyComponent.Tasker} path="/tasker" exact />
        <PrivateRoute component={LazyComponent.TaskerProfile} path="/tasker_profile" exact />
        <PrivateRoute component={LazyComponent.NewTasker} path="/new_tasker" exact />
        <PrivateRoute component={LazyComponent.Tasks} path="/tasks" exact />
        <PrivateRoute component={LazyComponent.Apartment} path="/apartment-community" exact />
        <PrivateRoute component={LazyComponent.Chatting} path="/chatting" exact />
        <PrivateRoute component={LazyComponent.MapDashboard} path="/" exact />
      </Switch>
    </Router>
  </Suspense>
);

export default Routes;
