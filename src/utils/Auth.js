// Service to check authentication for user and to signOut
import store from '../store'
const Auth = {
  signOut() {
    localStorage.removeItem("token");
  },
  isAuth() {
    return store.getState().auth.accessToken;
  }
};
export default Auth;
 
