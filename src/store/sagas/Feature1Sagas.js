import { call, put } from "redux-saga/effects";
import API from "../../network/apis/APIExample";
import * as ACTIONS from "../actions/Feature1";
import { dispatchSnackbarError } from "../../utils/Shared";
import { takeLatest } from "redux-saga/effects";
import * as TYPES from "../types/Feature1Types";
import History from '../../routes/History';

// Replace with your sagas
export function* feature1Saga() {
  try {
    const response = yield call(API.apiExampleRequest);
    yield put(ACTIONS.feature1ActionReceive(response.data));
  } catch (err) {
    dispatchSnackbarError(err.response.data);
  }
}

export function* adminLogin(action) {
  try {
    const {user} = action;
    const response = yield call(API.loginRequest, user);
    yield put(ACTIONS.adminLoginSuccess(response.data));
    History.push('/');
  } catch (err) {
    dispatchSnackbarError(err.response.data);
  }
}

export function* requestCategories() {
  try {
    const response = yield call(API.fetchCategories);
    yield put(ACTIONS.receiveTaskCategory(response.data));
  } catch (err) {
    dispatchSnackbarError(err.response.data);
  }
}

export function* requestApartments() {
  try {
    const response = yield call(API.fetchCommunities);
    yield put(ACTIONS.featureApartmentReceive(response.data));
  } catch (err) {
    dispatchSnackbarError(err.response.data);
  }
}

export function* requestLogOut() {
  try {
    const response = yield call(API.logoutRequest);
    yield put(ACTIONS.requestLogout(response.data));
  } catch (err) {
    dispatchSnackbarError(err.response.data);
  }
}

export function* saga1() {
  yield takeLatest(TYPES.GET_DATA_REQUEST, feature1Saga);
  yield takeLatest(TYPES.ADMIN_LOGIN_REQUEST, adminLogin);
  yield takeLatest(TYPES.GET_CATEGORIES_REQUEST, requestCategories);
  yield takeLatest(TYPES.ADMIN_LOGOUT_REQUEST, requestLogOut);
  yield takeLatest(TYPES.GET_APARTMENT_REQUEST, requestApartments);
}
