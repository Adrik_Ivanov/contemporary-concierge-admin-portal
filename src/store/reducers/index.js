import { combineReducers } from "redux";
import lang from "./Lang";
import loader from "./Loader";
import snackbar from "./Snackbar";
import Reducer from "./Reducer";

export default combineReducers({
  lang,
  loader,
  snackbar,
  auth: Reducer
});
