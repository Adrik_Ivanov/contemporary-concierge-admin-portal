import * as types from "../types/Feature1Types";

const INITIAL_STATE = {
  accessToken: '',
  user: null,
  error: '',
  categories: [],
  apartments: [],
};

// Replace with you own reducer
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.GET_DATA_RECEIVE:
      return {
        ...state,
        ...action.payload
      };
    case types.ADMIN_LOGIN_SUCCESS:
      return {
        ...state,
        accessToken: action.user.token,
        user: action.user.user,
      }
    case types.GET_CATEGORIES_SUCCESS:
      return {
        ...state,
        categories: action.categories
      };
    case types.GET_APARTMENT_SUCCESS:
      return {
        ...state,
        apartments: action.payload
      };
    case types.ADMIN_LOGIN_ERROR:
      return {
        ...state,
        error: action.error
      }
    case types.ADMIN_LOGOUT_SUCCESS:
        return INITIAL_STATE;
    default:
      return state;
  }
};
