import * as types from "../types/Feature1Types";

//Replace action name and update action types
export const feature1ActionRequest = () => ({
  type: types.GET_DATA_REQUEST
});

export const feature1ActionReceive = payload => ({
  type: types.GET_DATA_REQUEST,
  payload
});

export const featureApartmentRequest = () => ({
  type: types.GET_APARTMENT_REQUEST
});

export const featureApartmentReceive = payload => ({
  type: types.GET_APARTMENT_SUCCESS,
  payload
});

export const adminLogin = (user) => ({
  type: types.ADMIN_LOGIN_REQUEST,
  user
});

export const adminLoginSuccess = (user) => ({
  type: types.ADMIN_LOGIN_SUCCESS,
  user
});

export const adminLoginError = (error) => ({
  type: types.ADMIN_LOGIN_ERROR,
  error
});

export const requestTaskCategory = () => ({
  type: types.GET_CATEGORIES_REQUEST,
});

export const receiveTaskCategory = (categories) => ({
  type: types.GET_CATEGORIES_SUCCESS,
  categories
});

export const requestLogout = () => ({
  type: types.ADMIN_LOGOUT_SUCCESS,
});
