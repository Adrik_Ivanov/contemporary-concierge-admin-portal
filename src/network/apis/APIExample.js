import { axiosInstance } from "./index";
import store from "../../store";

// Replace endpoint and change api name
const apiExampleRequest = async () => {
  return await axiosInstance.get(`ENDPOINT`, { });
};

const loginRequest = async (user) => {
  return await axiosInstance.post(`/rest-auth/login/`, user);
};

const logoutRequest = async (param) => {
  return await axiosInstance.get(`/rest-auth/logout/` + param, {
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const fetchTasks = async (param) => {
  return await axiosInstance.get(`/api/v1/admin/task/` + param, {
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const fetchCategories = async () => {
  return await axiosInstance.get(`/api/v1/category/`, {
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const getCategories = async () => {
  return await axiosInstance.get(`/api/v1/admin/category/`, {
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const createCategory = async (data) => {
  return await axiosInstance.post(`/api/v1/admin/category/`,  data,{
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const updateCategory = async (pk, data) => {
  return await axiosInstance.put(`/api/v1/admin/category/` + pk + '/',  data,{
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}


const deleteCategory = async (pk) => {
  return await axiosInstance.delete(`/api/v1/admin/category/` + pk + '/',  {
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const fetchTasker = async (page) => {
  return await axiosInstance.get(`/api/v1/admin/taskerprofile/` + page, {
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const createTasker = async (data) => {
  return await axiosInstance.post(`/api/v1/admin/taskerprofile/`,  data,{
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const updateTasker = async (pk, data) => {
  return await axiosInstance.put(`/api/v1/admin/taskerprofile/` + pk + '/',  data,{
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const deleteTasker = async (pk) => {
  return await axiosInstance.delete(`/api/v1/admin/taskerprofile/` + pk + '/', {
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const fetchAvailableTaskers = async (pk, page = '') => {
  return await axiosInstance.get(`/api/v1/admin/taskerprofile/` + pk + '/' + page, {
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const fetchCustomers = async (page) => {
  return await axiosInstance.get(`/api/v1/admin/customerprofile/` + page, {
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const createCustomer = async (data) => {
  return await axiosInstance.post(`/api/v1/admin/customerprofile/`,  data,{
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const updateCustomer = async (pk, data) => {
  return await axiosInstance.put(`/api/v1/admin/customerprofile/` + pk + '/' + data,  {},{
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}


const deleteCustomer = async (pk) => {
  return await axiosInstance.delete(`/api/v1/admin/customerprofile/` + pk + '/',  {
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const fetchCommunities = async () => {
  return await axiosInstance.get(`/api/v1/admin/apartment/`, {
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const createCommunity = async (data) => {
  return await axiosInstance.post(`/api/v1/admin/apartment/`,  data,{
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const updateCommunity = async (pk, data) => {
  return await axiosInstance.put(`/api/v1/admin/apartment/` + pk + '/',  data,{
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const deleteCommunity = async (pk) => {
  return await axiosInstance.delete(`/api/v1/admin/apartment/` + pk + '/',  {
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const fetchMessages = async (page) => {
  return await axiosInstance.get(`/api/v1/admin/contact/` + page, {
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const createMessage = async (data) => {
  return await axiosInstance.post(`/api/v1/admin/contact/`,  data,{
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const updateMessage = async (pk, data) => {
  return await axiosInstance.put(`/api/v1/admin/contact/` + pk + '/',  data,{
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const deleteMessage = async (pk) => {
  return await axiosInstance.delete(`/api/v1/admin/contact/` + pk + '/',  {
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const fetchAdmins = async (page) => {
  return await axiosInstance.get(`/api/v1/administrator/` + page, {
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const createAdmin = async (data) => {
  return await axiosInstance.post(`/api/v1/administrator/`,  data,{
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const updateAdmin = async (pk, data) => {
  return await axiosInstance.put(`/api/v1/administrator/` + pk + '/',  data,{
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const deleteAdmin = async (pk) => {
  return await axiosInstance.delete(`/api/v1/administrator/` + pk + '/',  {
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const fetchRecentTasks = async (pk) => {
  return await axiosInstance.get(`/api/v1/admin/task/` + pk + '/',  {
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const assignTask = async (param) => {
  return await axiosInstance.post(`/api/v1/admin/task/?` + param , '', {
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const fetchPositions = async () => {
  return await axiosInstance.get(`/api/v1/taskerlocation/` , {
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const sendFCM = async (body) => {
  return await axiosInstance.post(`/api/v1/notification/` , body,{
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const uploadCustomerExcel = async (body) => {
  return await axiosInstance.post(`/api/v1/admin/invite-customer/` , body,{
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "multipart/form-data",
    },
  });
}

const fetchInvitedCustomers = async (param) => {
  return await axiosInstance.get(`/api/v1/admin/invite-customer/?` + param , {
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const resendInviteCustomer = async (id) => {
  return await axiosInstance.put(`/api/v1/admin/invite-customer/${id}/`, {},{
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

const deleteInviteCustomer = async (id) => {
  return await axiosInstance.delete(`/api/v1/admin/invite-customer/${id}/`, {
    headers: {
      Authorization: 'Bearer ' + store.getState().auth.accessToken,
      "Content-Type": "application/json",
    },
  });
}

export default {
  apiExampleRequest,
  loginRequest,
  logoutRequest,
  fetchCategories,
  getCategories,
  createCategory,
  updateCategory,
  deleteCategory,
  fetchTasks,
  fetchCommunities,
  createCommunity,
  updateCommunity,
  deleteCommunity,
  fetchMessages,
  createMessage,
  updateMessage,
  deleteMessage,
  fetchCustomers,
  createCustomer,
  updateCustomer,
  deleteCustomer,
  fetchTasker,
  createTasker,
  updateTasker,
  deleteTasker,
  fetchRecentTasks,
  fetchAvailableTaskers,
  assignTask,
  createAdmin,
  updateAdmin,
  fetchAdmins,
  deleteAdmin,
  fetchPositions,
  sendFCM,
  fetchInvitedCustomers,
  uploadCustomerExcel,
  resendInviteCustomer,
  deleteInviteCustomer
};
