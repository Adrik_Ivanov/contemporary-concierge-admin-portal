import * as firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyA9Lu70jaAPpgnBsm0zN88HKAZ268-hwlg",
    authDomain: "contemporary-conierge.firebaseapp.com",
    databaseURL: "https://contemporary-conierge.firebaseio.com",
    projectId: "contemporary-conierge",
    storageBucket: "contemporary-conierge.appspot.com",
    messagingSenderId: "546326459365",
    appId: "1:546326459365:web:255051191d60716f57350c",
    measurementId: "G-JMTHYQQ7B6"
};

firebase.initializeApp(firebaseConfig);

const database = firebase.firestore();
const auth = firebase.auth();

export {
    database,
    auth,
};
